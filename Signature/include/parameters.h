﻿/*parameters.h
 * Auteur : MARREF KHALIL
 * Date de création : 28/02/2024
 * Description : That header file contain list of variables and parameters retreived from config file 
 */


#pragma once
#include <iostream>
#include <cryptoki_v2.h>
#include <vector>
#include <ini.h>

using namespace std;

#define DEFAULT_CONFIG_FILE "\\config\\config.ini"
#define P11_VARIABLE_LENGTH 32;
#define LABELS 64; 

class Parameters {

public:
	std::string P11_VARIABLE_STR{ NULL };
	char P11_VARIABLE_CHAR[20]{ NULL };
	short slotID = -1;
	std::string CONFIG_SECTION{ NULL };
	std::string HSM_SECTION{ NULL };
	std::string LOGS_SECTION{ NULL };
	char* P11_VARIABLE{ NULL };
	std::string LUNA_DIRECTORY{ NULL };
	std::string LUNA_CONFIGFILE{ NULL };
	std::string LUNACLIENT = "LunaSA Client";
	std::string SERVER = "ServerName";
	string HSM_IPADDRESS{ NULL };
	inih::INIReader iniFile{ DEFAULT_CONFIG_FILE };
	Parameters();

private:
	
	
	int getHSMIPAddress();
	int getSlot();
	int getPIN();
	int getCryptoki();
	int getLunaDirectory();
	int getLunaConfigFile();
	int getKeyLength();
	int getLogFile();
	int getSeverityLevel();
	int getSyslogServer();
	int getSyslogPort();
	


};

class parametersEmpty : public exception
{
public:
	parametersEmpty(string parameter) : parameter{ parameter } {};
	
	const char* what() const noexcept override  {
		return "[ERROR] Parameters are empty ";
	}

private:
	string  parameter { 0 };
};