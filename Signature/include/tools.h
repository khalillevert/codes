#pragma once
#pragma once
#include <iostream>
#include <windows.h>
#include <cryptoki_v2.h>
#include <parameters.h>
#include <vector>
#include <iomanip>

using namespace std;

inline void display() {
	std::cout << "*******************************************" << std::endl;
	std::cout << "*		PDF SIGNATURE					*" << std::endl;
	std::cout << "*		Release: Draft					*" << std::endl;
	std::cout << "*		Verion:  0.1					*" << std::endl;
	std::cout << "*		Author:  KHALIL MARREF			*" << std::endl;
	std::cout << "*******************************************" << std::endl;
}
inline void DisplayBlock()
{
	std::cout << endl;
	std::cout << endl;
	std::cout << "#############################" << endl;
	std::cout << "#           END             #" << std::endl;
	std::cout << "#        TAPE ENTER         #" << std::endl;
	std::cout << "#############################" << endl;
	char a;
	std::cin >> a;
}

template<typename T>
void testingShow(const T& var)
{
	cout << "[Testing Message]" << var << endl;
}

void printHexa(CK_BYTE* tab)
{
	for (int i = 0; i < sizeof(tab); ++i) {
		std::cout << "CK_BYTE[" << i << "] = 0x" << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(tab[i]) << std::endl;

		cout << endl;
	}
}
